<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('competitions', 'Api\CompetitionsController', ['only' => ['index', 'show', 'store', 'update']]);
Route::resource('competitions/{competition}/applications', 'Api\ApplicationsController', ['only' => ['store', 'index']]);
Route::delete('applications/{id}', 'Api\ApplicationsController@destroy')->name('applications.destroy');