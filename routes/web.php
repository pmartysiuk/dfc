<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Web\IndexController@index');
Route::resource('competitions', 'Web\CompetitionsController', ['only' => ['show', 'store']]);


Route::get('/admin/competitions/{id}/applications', 'Web\Admin\ApplicationsController@index')->name('competitions.applications.index');
