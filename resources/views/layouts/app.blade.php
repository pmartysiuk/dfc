<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dog Frisbee - @yield('title')</title>

    <link href="<?=asset('/css/app.css')?>" rel="stylesheet" type="text/css">

    @yield('css')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript" src="<?=asset('/js/app.js')?>"></script>

</head>

<body>

<div class="container">
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            @include('navigation.top')
            {{--@include('navigation.side')--}}
        </nav>

        <div id="page-wrapper">
            @yield('content')
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: none;">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#login" aria-controls="login" role="tab" data-toggle="tab">Вход</a></li>
                    <li role="presentation"><a href="#register" aria-controls="register" role="tab" data-toggle="tab">Регистрация</a></li>
                </ul>
            </div>
            <div class="modal-body">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane" id="login">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Remember me
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default">Sign in</button>
                                </div>
                            </div>
                        </form>л
                    </div>
                    <div role="tabpanel" class="tab-pane active" id="register">
                        {{--<form class="form-horizontal">--}}
                        <form>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>Имя</label>
                                        <input name="name" type="text" class="form-control" placeholder="Имя">
                                    </div>
                                    <div class="col-lg-6">
                                        <label>Фамилия</label>
                                        <input name="surname" type="text" class="form-control" placeholder="Фамилия">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>Email</label>
                                        <input name="email" type="text" class="form-control" placeholder="Email">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>Дата рождения</label>
                                        <input name="birthday" type="text" class="form-control" placeholder="Дата рождения">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Телефон</label>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <input name="phone-code" type="text" class="form-control" placeholder="Код">
                                    </div>
                                    <div class="col-lg-8">
                                        <input name="phone" type="text" class="form-control" placeholder="Телефон">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>Социальная сеть</label>
                                        <input name="social-network" type="text" class="form-control" placeholder="Социальная сеть">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>Есть ли у вас собака</label>
                                        <input name="dog-exists" type="text" class="form-control" placeholder="Социальная сеть">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>Порода собаки</label>
                                        <input name="dog-breed" type="text" class="form-control" placeholder="Социальная сеть">
                                    </div>
                                    <div class="col-lg-6">
                                        <label>Кличка собаки</label>
                                        <input name="dog-name" type="text" class="form-control" placeholder="Кличка собаки">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Дата рождения собаки</label>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <input name="dog-bitrh-month" type="text" class="form-control" placeholder="Месяц">
                                    </div>
                                    <div class="col-lg-6">
                                        <input name="dog-bitrh-year" type="text" class="form-control" placeholder="Год">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>Пароль</label>
                                        <input name="password" type="password" class="form-control" placeholder="Пароль">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <label>Подтверждение пароля</label>
                                        <input name="password-confirmation" type="password" class="form-control" placeholder="Подтверждение пароля">
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<script type="text/javascript">
    $(function(){
	    $('#button-enter').click(function(){
		    $('#myModal').modal('show');
        });
    });
</script>

@yield('js')

</body>
</html>