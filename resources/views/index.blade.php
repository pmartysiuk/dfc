@extends('layouts.app')

@section('title', 'Главная')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Календарь соревнований по дог-фрисби на {{date('Y')}} год</h1>
        </div>

        @if (empty($competitions))
            <h4>{{ trans('competitions.not-planned') }}</h4>
            <img src="https://media.giphy.com/media/jou4Cd2mx1lGU/giphy.gif" />
        @else
            <table class="table">
                @foreach ($competitions as $competition)
                <tr>
                    <td>
                        @if($competition['status'] != 1)<a href="{{route('competitions.show', ['id' => $competition['id']])}}">@endif

                            {{date('j', strtotime($competition['begin_date']))}} {{trans('date.' . date('F', strtotime($competition['begin_date'])))}}&nbsp;&mdash;&nbsp;
                            @if(!empty($competition['title']))
                                {{$competition['title']}},
                            @endif
                            {{ $competition['type'] }}, {{ $competition['city']}}

                        @if($competition['status'] != 1)</a>@endif
                    </td>
                </tr>
                @endforeach
            </table>
        @endif
    </div>
@endsection