<div class="modal fade" id="create-application-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Заявка на участие в соревновании "{{$competition->title}}"</h4>
            </div>
            <div class="modal-body">
                <form id="create-application">

                    <div class="alert alert-success collapse" id="create-application-success" role="alert">Ваша заявка отправлена!</div>
                    <div class="alert alert-danger collapse" id="create-application-error" role="alert">В заявке есть ошибки!</div>
                    <div class="alert alert-danger collapse" id="create-application-fail" role="alert">Что-то пошло не так!</div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="firstName" class="control-label">Имя<span style="color: red"> *</span></label>
                                <input type="text" class="form-control" id="first-name" placeholder="Имя" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="lastName" class="control-label">Фамилия<span style="color: red"> *</span></label>
                                <input type="text" class="form-control" id="last-name" placeholder="Фамилия" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="control-label">Email<span style="color: red"> *</span></label>
                        <input type="email" class="form-control" id="email" placeholder="Email" required>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="phone" class="control-label">Телефон <i>(+375 XX XXXXXXX)</i><span style="color: red"> *</span></label>
                                <input type="tel" class="form-control" id="phone" placeholder="Телефон" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="birth_date" class="control-label">Дата рождения <i>(дд.мм.гггг)</i><span style="color: red"> *</span></label>
                                <input type="text" class="form-control" id="birth_date" placeholder="Дата рождения" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="petName" class="control-label">Кличка собаки<span style="color: red"> *</span></label>
                        <input type="text" class="form-control" id="pet-name" placeholder="Кличка собаки" required>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="petBreed" class="control-label">Порода собаки<span style="color: red"> *</span></label>
                                <input type="text" class="form-control" id="pet-breed" placeholder="Порода собаки" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="petBirthDate" class="control-label">Дата рождения собаки <i>(мм.гггг)</i><span style="color: red"> *</span></label>
                                <input type="text"  class="form-control" id="pet-birth-date" placeholder="Дата рождения собаки" required>
                            </div>
                        </div>
                    </div>

                    <p><strong>Дисциплины</strong></p>
                    @foreach($competition->disciplines as $discipline)
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" value="{{$discipline['id']}}" name="disciplines[]"> {{$discipline['title']}}
                            </label>
                        </div>
                    @endforeach

                    <div class="form-group">
                        <label for="comment" class="control-label">Комментарий</label>
                        <textarea class="form-control" id="comment" placeholder="Комментарий"></textarea>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-primary" id="create-application-submit">Подать заявку</button>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript">
$(function(){

    function resetForm(form) {
        form.find('.alert').hide();
        form.find('.has-error').removeClass('has-error');
    }

    $('#create-application-submit').click(function () {
        const form = $('#create-application');

        let data = {
            "first_name": form.find('#first-name').val(),
            "last_name": form.find('#last-name').val(),
            "email": form.find('#email').val(),
            "phone": form.find('#phone').val(),
            "birth_date": form.find('#birth_date').val(),
            "pet_breed":  form.find('#pet-breed').val(),
            "pet_name": form.find('#pet-name').val(),
            "pet_birth_date": form.find('#pet-birth-date').val(),
            "comment": form.find('#comment').val(),
        };

        data["disciplines"] = form.find('input[name="disciplines[]"]:checked').map(function(){
            return $(this).val();
        }).get();

        $('#create-application-modal').on('hidden.bs.modal', function (e) {
            resetForm(form);
        });

        $.ajax({
            method: "POST",
            data: data,
            url: "{{ route('applications.store', ['competition' => $competition->id]) }}",
            beforeSend: function() {
                resetForm(form);
            },
            statusCode: {
                201: function(data) {
                    $('#create-application-success').show();
                    form.find(':input')
                        .not(':button, :submit, :reset, :hidden')
                        .val('')
                        .prop('checked', false)
                        .prop('selected', false);
                    window.location.reload(true);
                },
                422: function(data) {
                    $('#create-application-error').show();
                    if (data.responseJSON.errors === undefined) {
                        return false;
                    }
                    $.each(data.responseJSON.errors, function (field, value) {
                        let fieldId = field.replace(/_/g, '-');
                        form.find('#' + fieldId).parent('.form-group').addClass('has-error');
                    });
                },
                500: function(data) {
                    $('#create-application-fail').show();
                }
            }
        }).done(function() {
            // TODO: add scroll to top or validation error
        });

    });
});
</script>


