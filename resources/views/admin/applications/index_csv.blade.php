<?php

$data = [];

$headers = [
    'Имя',
    'Email',
    'Телефон',
    'Дата рождения',
    'Кличка собаки',
    'Порода собаки',
    'Дата рождения собаки',
    'Комментарий'
];


foreach ($competition['disciplines'] as $discipline) {
    $headers[] = $discipline['title'];
}

$headers += ['Дата создания заявки', 'Стоимость участия'];

$data[] = $headers;


foreach ($applications as $application) {

    $row = [
        $application['applicant_name'],
        $application['applicant_email'],
        $application['applicant_phone'],
        date('d.m.Y', strtotime($application['applicant_birth_date'])),
        $application['pet_name'],
        $application['pet_breed'],
        $application['pet_birth_date'],
        $application['comment'],
    ];

    $applicationDisciplineIds = array_pluck($application['disciplines'], 'id');
    foreach ($competition['disciplines'] as $discipline) {
        $row[] = in_array($discipline['id'], $applicationDisciplineIds) ? '+' : '-';
    }

    $row[] = $application['created_at'];
    $row += ['', ''];

    $data[] = $row;
}
echo "\xEF\xBB\xBF";
foreach ($data as $row) {
    echo implode(',', $row) . PHP_EOL;
}



