@extends('layouts.app')

@section('title', 'Вот это заявочки')

@section('content')
    <div class="row">
        <div class="col-sm-8">
            <h1 class="page-header">Заявки на соревнование {{ $competition['title'] }}</h1>
        </div>
        <div class="col-sm-4">
            <div class="page-header">
                <button class="btn btn-success download-applications"
                    data-action="{{ route('competitions.applications.index', ['id' => $competition['id']]) }}">Сохранить в csv</button>
            </div>
        </div>
    </div>

    <div class="row">
        @if (empty($applications))
            <img src="https://media.giphy.com/media/jou4Cd2mx1lGU/giphy.gif" />
        @else
            <table class="table">
                <tr>
                    <th>Имя</th>
                    <th>Email</th>
                    <th>Телефон</th>
                    <th>Дата рождения</th>
                    <th>Кличка собаки</th>
                    <th>Порода собаки</th>
                    <th>Дата рождения собаки</th>
                    <th>Комментарий</th>
                    @foreach($competition['disciplines'] as $discipline)
                        <th>{{$discipline['title']}}</th>
                    @endforeach
                    <th>Дата создания заявки</th>
                    <th></th>
                </tr>

                @foreach ($applications as $application)
                <tr>
                    <td>{{ $application['applicant_name'] }}</td>
                    <td>{{ $application['applicant_email'] }}</td>
                    <td>{{ $application['applicant_phone'] }}</td>
                    <td>{{ date('d.m.Y', strtotime($application['applicant_birth_date'])) }}</td>
                    <td>{{ $application['pet_name'] }}</td>
                    <td>{{ $application['pet_breed'] }}</td>
                    <td>{{ $application['pet_birth_date'] }}</td>
                    <td>{{ $application['comment'] }}</td>
                    @php
                        $applicationDisciplineIds = array_pluck($application['disciplines'], 'id');
                    @endphp
                    @foreach($competition['disciplines'] as $discipline)
                        <td>
                            @if (in_array($discipline['id'], $applicationDisciplineIds))
                                <span class="glyphicon glyphicon-ok-sign" style="color: #4cae4c;" aria-hidden="true"></span>
                            @else
                                <span class="glyphicon glyphicon-remove-sign" style="color: #adadad" aria-hidden="true"></span>
                            @endif
                        </td>
                    @endforeach
                    <td>{{ $application['created_at'] }}</td>
                    <td>
                        <button class="btn btn-danger delete-application"
                                data-action="{{ route('applications.destroy', ['id' => $application['id']]) }}">X</button>
                    </td>
                </tr>
                @endforeach
            </table>
        @endif
    </div>


    <script type="text/javascript">

        $('.download-applications').on('click', function () {
            $.ajax({
                url: $(this).data('action'),
                method: 'GET',
                headers: {
                    Accept: "text/csv"
                },
                xhrFields: {
                    responseType: 'blob'
                },
                success: function (data, textStatus, request) {
                    let a = document.createElement('a');
                    let url = window.URL.createObjectURL(data);
                    a.href = url;

                    let fileName = request.getResponseHeader('Content-Disposition').replace(/.*''/, '');
                    a.download = decodeURIComponent(fileName);
                    a.click();
                    window.URL.revokeObjectURL(url);
                }
            });
        });

        $('.delete-application').click(function () {

            if (!confirm('Вы точно хотите удалить эту заявку?')) {
                return false;
            }

            let $this = $(this);
            $.ajax({
                method: "DELETE",
                url: $this.data('action'),
                statusCode: {
                    204: function(data) {
                        $this.parents('tr').remove();
                    }
                }
            });
        });
    </script>

@endsection