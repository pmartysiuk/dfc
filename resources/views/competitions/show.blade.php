@extends('layouts.app')

@section('title', $competition->full_title)

@section('js')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_KEY') }}&callback=initMap"
            type="text/javascript"></script>
@endsection

@section('content')

    @if($competition === null)
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Соревнование не найдено</h1>
            </div>
        </div>
    @else

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">{{$competition->full_title}}</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <p>Всем привет!
                    Началась подача на визы, члены федерации, с оплаченным взносом до текущего месяца включительно,
                    могут подать заявку, заполнив таблицу (https://docs.google.com/spreadsheets/d/1NDnVYmg0Q3edr..) до
                    25.03.2018

                    Заполняйте поля по образцу 1-й строки. Указывайте даты в формате гг-мм-дд.

                    Если вы не в Федерации, то присылайте заявку на вступление до 25 марта.
                    Как вступить в Федерацию ->http://bfdf.by/federation/join/

                    Чтобы получить подтверждение от Федерации на получение приглашения и визы у вас должны быть оплачены
                    взносы до текущего месяца включительно.

                    Информация по взносам http://bfdf.by/federation/fees/
                    Подробнее можно узнать информацию у Катерины (https://vk.com/id38108360)

                    За информацией по визам обращаться к Максиму</p>
            </div>
        </div>

        <!-- Button trigger modal -->
        <div class="row">
            <div class="col-sm-12 text-center">
                <button type="button" class="btn btn-success btn-lg" data-toggle="modal"
                        data-target="#create-application-modal">
                    Подать заявку
                </button>
                <p>Цена снижена при подаче заявки до {{ $competition->registration_end_date }} </p>
            </div>
        </div>

        @include('modals.application', ['competition' => $competition])

        @if (isset($competition->coordinates['latitude']))
            <br>
            <div class="col-lg-12">
                <div class="row">
                    <div id="map" style="height: 250px; width: 100%;"></div>
                    <script>
                        function initMap() {
                            var place = {
                                lat: {{ $competition->coordinates['latitude'] }},
                                lng: {{$competition->coordinates['longitude']}} };
                            var map = new google.maps.Map(document.getElementById('map'), {
                                zoom: 14,
                                center: place
                            });
                            var marker = new google.maps.Marker({
                                position: place,
                                map: map
                            });
                        }
                    </script>
                </div>
                <br>
                <div class="row">
                    <i>{{$competition->coordinates['latitude']}}, {{$competition->coordinates['longitude']}}</i>
                </div>
                <br>
            </div>


        @endif

        <div class="row">
            <div class="col-lg-12">
                <p><strong><em>Дата проведения: </em></strong>{{ $competition->begin_date }}</p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <p><strong><em>Место проведения: </em></strong>{{ $competition->full_address }}</p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <p><strong><em>Организатор: </em></strong>{{ $competition->organizer }}</p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <p><strong><em>Состав судей: </em></strong></p>
                <ol>
                    @foreach($competition->judge as $judge)
                        <li>{{ $judge }}</li>
                    @endforeach
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <p><strong><em>Дисциплины для участия: </em></strong></p>
                <ul>
                    @foreach($competition->disciplines as $discipline)
                        <li><a target="_blank" href="{{ $discipline['rules'] }}">{{ $discipline['title'] }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <p><strong><em>Партнеры соревнования: </em></strong></p>
                <ol>
                    @foreach($competition->sponsor as $sponsor)
                        <li>{{ $sponsor['title'] }} <a href="{{ $sponsor['url'] }}">{{ $sponsor['url'] }}</a></li>
                    @endforeach
                </ol>
                @foreach($competition->sponsor as $sponsor)
                    <img width="100" src="/{{$sponsor['logo']}}">
                @endforeach
            </div>
        </div>

        <div class="row">
            <div class="row">
                <div class="col-lg-12">
                    <h2>Заявки на соревнование {{ $competition->title }}</h2>

                    @foreach($competition->disciplines as $discipline)
                        <h3>{{$discipline['title']}}</h3>

                        @if(empty($disciplineApplications[$discipline['id']]))
                            <p>нет заявок</p>
                            @continue
                        @endif

                        <table class="table table-hover table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Имя</th>
                                <th>Кличка собаки</th>
                                <th>Порода собаки</th>
                                <th>Дата рождения собаки</th>
                            </tr>
                            </thead>

                            @foreach($disciplineApplications[$discipline['id']] as $disciplineApplication)
                                <tr>
                                    <td>{{ $disciplineApplication['applicant_name'] }}</td>
                                    <td>{{ $disciplineApplication['pet_name'] }}</td>
                                    <td>{{ $disciplineApplication['pet_breed'] }}</td>
                                    <td>{{ $disciplineApplication['pet_birth_date'] }}</td>
                                </tr>
                            @endforeach
                        </table>
                    @endforeach
                </div>
            </div>
        </div>

    @endif
@endsection