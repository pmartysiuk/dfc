@extends('layouts.app')

@section('title', 'Соревнование')

@section('content')

    <div class="col-sm-8 blog-main">
        <h1>Publish new post</h1>


        <form method="POST" action="/competitions">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="title">Создание нового соревнования</label>
                <input type="text" name="title" class="form-control" id="competition_title" placeholder="">
            </div>
            <div class="form-group">
                <select name="status", id="competition_status">
                    <option value="1">Audi</option>
                </select>




            </div>




            <button type="submit" class="btn btn-primary">Publish</button>




            'status' => 'required|int|in:1,2,3,4,5',
            'type_id' => 'required|int|in:1,2,3,4,5',
            'begin_date' => 'required|date_format:"Y-m-d"',
            'end_date' => 'required|date_format:"Y-m-d"|after_or_equal:begin_date',
            'city' => 'required|string|max:256',
            'title' => 'required|string|max:1024',

        </form>
        @include('layouts.errors')
    </div>
@endsection