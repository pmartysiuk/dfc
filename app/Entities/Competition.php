<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Competition extends Model
{
    const STATUS_PLANNED = 1;
    const STATUS_REGISTRATION_OPENED = 2;
    const STATUS_REGISTRATION_CLOSED = 3;
    const STATUS_STARTED = 4;
    const STATUS_CLOSED = 5;
    const STATUS_ARCHIVED = 6;

    protected $fillable = ['status', 'begin_date', 'end_date', 'city', 'title', 'created_by', 'type_id'];

    public function getApplicationsAttribute()
    {
        return Application::select('applications.*')
            ->join(
            'competition_discipline_application as cda',
            'applications.id',
            '=',
            'cda.application_id'
        )->where('cda.competition_id', $this->id)
            ->groupBy('applications.id')
            ->get();
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActual($query)
    {
        return $query->whereIn('status', [
            self::STATUS_PLANNED,
            self::STATUS_REGISTRATION_OPENED,
            self::STATUS_REGISTRATION_CLOSED,
            self::STATUS_STARTED,
            self::STATUS_CLOSED,
        ]);
    }

    public function scopeArchived($query)
    {
        return $query->where('status', self::STATUS_ARCHIVED);
    }

    public function scopeBeginDateYear($query, int $year)
    {
        return $query->whereRaw("YEAR(begin_date) = $year");
    }

    public function disciplines()
    {
        return $this->belongsToMany('\App\Entities\Discipline');
    }

    public function type()
    {
        return $this->hasOne('App\Entities\CompetitionType', 'id', 'type_id');
    }
}
