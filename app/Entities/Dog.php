<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $breed
 * @property string $sex
 * @property Carbon $birthday
 * @property string $avatar
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */

class Dog extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name',
        'breed',
        'sex',
        'birthday',
        'avatar'
    ];

    protected $dates = [
        'birthday',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'name' => 'string',
        'breed'=> 'string',
        'sex'=> 'string',
        'avatar'=> 'string',
        'birthday'=> 'date',
    ];


    public function owner()
    {
        $this->belongsTo('App\Entities\User');
    }
}
