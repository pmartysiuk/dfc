<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


/**
 * @property integer $id
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string $password
 * @property string $avatar
 * @property string $social_network
 * @property Carbon $birthday
 * @property string $remember_token
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name',
        'surname',
        'email',
        'password',
        'avatar',
        'social_network',
        'birthday',
        'phone',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $dates = [
        'birthday',
        'created_at',
        'updated_at',
    ];

    protected $casts = [
        'name' => 'string',
        'surname'=> 'string',
        'email'=> 'string',
        'password'=> 'string',
        'avatar'=> 'string',
        'social_network'=> 'string',
        'birthday'=> 'date',
    ];

    public function dogs()
    {
        return $this->hasMany('App\Entities\Dog');
    }
}
