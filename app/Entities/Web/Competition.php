<?php

namespace App\Entities\Web;

class Competition
{
    protected $competition;

    public function __construct(array $competition)
    {
        $this->competition = $competition;
    }

    public function getFullTitleAttribute()
    {
        return $this->competition["type"] . ' "' . $this->competition["title"] .'"';
    }

    public function getFullAddressAttribute()
    {
//        TODO check indexes existence
        return $this->competition["city"] .", ".
            $this->competition["address"] .", ".
            $this->competition["place"];
    }


    /**
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        $getter = "get" . studly_case($name) . "Attribute";

        if (method_exists($this, $getter)) {
            return $this->$getter();
        }

        if (array_key_exists($name, $this->competition)) {
            return $this->competition[$name];
        }

        throw new \Exception("Property $name doesn't exist!");
    }


}