<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Discipline extends Model
{
    public function competitions()
    {
        return $this->belongsToMany('\App\Entities\Competition');
    }

    public function toArray()
    {
        $result = parent::toArray();
        if (array_key_exists('pivot', $result)) {
            unset($result['pivot']);
        }

        return $result;
    }
}
