<?php

namespace App\Entities;


use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function getInfoAttribute()
    {
        return json_decode($this->attributes['info'], true);
    }

    public function setApplicantBirthDateAttribute($date)
    {
        return $this->attributes['applicant_birth_date'] = date('Y-m-d', strtotime($date));
    }

    public function getDisciplinesAttribute()
    {
        return Discipline::select('disciplines.*')
            ->join(
                'competition_discipline_application as cda',
                'disciplines.id',
                '=',
                'cda.discipline_id'
            )->where('cda.application_id', $this->id)
            ->get();
    }
}