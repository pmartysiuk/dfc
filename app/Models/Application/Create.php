<?php

namespace App\Models\Application;

use App\Entities\Application;
use App\Entities\Competition;
use App\Mail\ApplicationSentAdmin;
use Validator;

class Create
{
    protected $errors;
    protected $data = [];
    protected $competition;
    protected $validator;

    public function __construct(array $data, Competition $competition)
    {
        $this->data = $data;
        $this->competition = $competition;
    }

    public function execute()
    {
        if (!$this->isValid()) {
            return false;
        }

        \DB::beginTransaction();

        $application = new Application([
            'applicant_name' => $this->data['first_name'] . ' ' . $this->data['last_name'],
            'applicant_email' => $this->data['email'],
            'applicant_phone' => $this->data['phone'],
            'applicant_birth_date' => $this->data['birth_date'],
            'info' => json_encode([
                'pet_breed' => $this->data['pet_breed'],
                'pet_name' => $this->data['pet_name'],
                'pet_birth_date' => $this->data['pet_birth_date'],
            ]),
            'comment' => $this->data['comment'],
        ]);
        $application->save();

        foreach ($this->data['disciplines'] as $discipline) {
            \DB::insert(
                'insert into competition_discipline_application 
                    (competition_id, discipline_id, application_id) values (?, ?, ?)',
                [$this->competition->id, $discipline, $application->id]
            );
        }

        \DB::commit();

//        \Mail::send(new ApplicationSentAdmin($application, $this->competition));

        return $application;
    }

    protected function rules()
    {
        return [
            'first_name' => 'required|string|max:256',
            'last_name' => 'required|string|max:256',
            'phone' => 'required|string|max:16',
            'birth_date' => 'required|date|date_format:d.m.Y',
            'email' => 'required|email',
            'pet_breed' => 'required|string|max:256',
            'pet_name' => 'required|string|max:256',
            'pet_birth_date' => 'required|string|date_format:m.Y',
            'comment' => 'nullable|string|max:1000',
            'disciplines' => 'required|array',
        ];
    }

    public function errors()
    {
        $this->validate();

        return $this->errors;
    }

    public function isValid()
    {
        $errors = $this->errors();

        return empty($errors);
    }

    public function validator()
    {
        $this->validate();

        return $this->validator;
    }

    protected function validate()
    {
        if ($this->errors !== null) {
            return;
        }

        $this->validator = Validator::make($this->data, $this->rules());
        $this->errors = $this->validator->fails() ? $this->validator->errors() : [];
    }
}