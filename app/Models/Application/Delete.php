<?php

namespace App\Models\Application;

class Delete
{
    protected $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public function execute()
    {
        \DB::beginTransaction();
        \DB::delete('delete from applications where id = ?', [$this->id]);
        \DB::delete('delete from competition_discipline_application where application_id = ?', [$this->id]);
        \DB::commit();
    }
}