<?php

namespace App\Http\Controllers\Web;


use App\Entities\Web\Competition;
use Illuminate\Http\Request;

class CompetitionsController extends WebController
{

    public function show(Request $request, int $id)
    {
        $request = Request::create(
            "/api/competitions/$id",
            'GET',
            [],
            $request->cookie(),
            $request->file(),
            $request->server()
        );
        $request->header('Accept', 'application/json');

        $response = app()->handle($request);

        $competition = $response->getStatusCode() == 200 ? json_decode($response->getContent(), true) : null;

        if ($competition) {
            $competition = new Competition($competition);
        }

        $disciplineApplications = [];

        foreach ($competition->applications as $application) {
            foreach ($application['disciplines'] as $discipline) {
                $disciplineApplications[$discipline['id']][] = $application;
            }
        }

        return view('competitions.show', compact('competition', 'disciplineApplications'));
    }

    public function store(Request $request)
    {
        $request = Request::create(
            "/api/competitions",
            'POST',
            [],
            $request->cookie(),
            $request->file(),
            $request->server()
        );
        $request->header('Accept', 'application/json');
        $response = app()->handle($request);

        $competition = $response->getStatusCode() == 201 ? json_decode($response->getContent(), true) : null;

        if ($competition) {
            $competition = new Competition($competition);
        }

        return view('competitions.show', compact('competition'));
    }

}