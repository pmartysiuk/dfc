<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;

class IndexController extends WebController
{
    public function index(Request $request)
    {
        $request = Request::create(
            '/api/competitions',
            'GET',
            [
                'status' => 'actual',
                'begin_date_year' => (int) date('Y')
            ],
            $request->cookie(),
            $request->file(),
            $request->server()
        );
        $request->header('Accept', 'application/json');

        $response = json_decode(app()->handle($request)->getContent(), true);

        $competitions = $response['data'] ?? [];

        return view('index', compact('competitions'));
    }
}