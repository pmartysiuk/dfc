<?php

namespace App\Http\Controllers\Web\Admin;

use App\Entities\Competition;
use App\Http\Controllers\Web\WebController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ApplicationsController extends WebController
{
    public function index(Request $request, int $id)
    {
        $request = Request::create(
            "/api/competitions/$id",
            'GET',
            [],
            $request->cookie(),
            $request->file(),
            $request->server()
        );
        $request->header('Accept', 'application/json');

        $competition = json_decode(app()->handle($request)->getContent(), true);

        $applications = $competition['applications'];
        $disciplines = $competition['disciplines'];


        if ($request->accepts('text/csv') && !$request->accepts('text/html')) {
            $data = view(
                'admin.applications.index_csv',
                compact('competition', 'applications', 'disciplines')
            )->render();
            $tmpFilename = storage_path('app/files/applications/' . time());
            file_put_contents($tmpFilename,  $data);

            $filename = "Заявки на турнир {$competition['title']}.csv";

            return response()
                ->download(
                    $tmpFilename,
                    $filename,
                    [
                        'Content-Description' => 'File Transfer',
                        'Content-Type' => 'application/octet-stream',
                        'Content-Transfer-Encoding' => 'binary',
                        'X-File-Name' => urlencode($filename),
                    ]
                )
                ->deleteFileAfterSend(true);
        }

        return view('admin.applications.index', compact('competition', 'applications', 'disciplines'));
    }
}