<?php

namespace App\Http\Controllers\Api;


use App\Entities\Application;
use App\Entities\Competition;
use App\Models\Application\Create;
use App\Models\Application\Delete;
use App\Transformers\Api\ApplicationsTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ApplicationsController extends ApiController
{

    /**
     * @var ApplicationsTransformer
     */
    protected $applicationsTransformer;

    /**
     * @param ApplicationsTransformer $applicationsTransformer
     */
    function __construct(ApplicationsTransformer $applicationsTransformer)
    {
        $this->applicationsTransformer = $applicationsTransformer;
    }

    public function store(Request $request, Competition $competition)
    {
        $createModel = new Create($request->all(), $competition);
        $application = $createModel->execute();

        if (!$application) {
            return response()->json(['errors' => $createModel->errors()], 422);
        }

        return response(compact(['application']), 201);
    }

    public function destroy($id)
    {
        $deleteModel = new Delete($id);
        $deleteModel->execute();

        return response('', 204);
    }

    public function index(Request $request, Competition $competition)
    {
        return response($this->applicationsTransformer->transformCollection($competition->applications));
    }
}