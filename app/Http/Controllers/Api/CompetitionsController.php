<?php

namespace App\Http\Controllers\Api;

use App\Entities\Competition;
use App\Transformers\Api\CompetitionsTransformer;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class CompetitionsController extends ApiController
{

    /**
     * @var CompetitionsTransformer
     */
    protected $competitionsTransformer;

    /**
     * @param CompetitionsTransformer $competitionsTransformer
     */
    function __construct(CompetitionsTransformer $competitionsTransformer)
    {
        $this->competitionsTransformer = $competitionsTransformer;
    }


    /**
     * Display a listing of the competitions.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'in:actual,archived',
            'begin_date_year' => 'integer|min:0',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $competitions = Competition::oldest('begin_date');

        if ($request->has('status')) {
            $competitions->{$request->get('status')}();
        }

        if ($request->has('begin_date_year')) {
            $competitions->beginDateYear($request->get('begin_date_year'));
        }

        $competitions = $competitions->paginate(15)->items();
        $competitions['data'] = $this->competitionsTransformer->transformCollection($competitions);

        return response()->json($competitions);
    }

    public function show(int $id)
    {
        //проверить что id - int - 404
        $competition = Competition::find($id);
        if (!$competition) {
            throw new ModelNotFoundException('Competition not found');
        }

        $competition = $this->competitionsTransformer->transform($competition);

        return response()->json($competition);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'required|int|in:1,2,3,4,5',
            'type_id' => 'required|int|in:1,2,3,4,5',
            'begin_date' => 'required|date_format:"Y-m-d"',
            'end_date' => 'required|date_format:"Y-m-d"|after_or_equal:begin_date',
            'city' => 'required|string|max:256',
            'title' => 'required|string|max:1024',
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $competition = new Competition($request->all());
        $competition->save();

        return response()->json($competition)->setStatusCode(201);
    }

    public function update(Request $request, int $id)
    {
        $validator = Validator::make($request->all(), [
            'status' => 'nullable|int|in:1,2,3,4,5',
            'type_id' => 'nullable|int|in:1,2,3,4,5',
            'begin_date' => 'nullable|date_format:"Y-m-d"',
            'end_date' => 'nullable|date_format:"Y-m-d"|after_or_equal:begin_date',
            'city' => 'nullable|string|max:256',
            'address' => 'nullable|string|max:256',
            'title' => 'required|string|max:1024',
            'description' => 'nullable|string|max:1024',
            'organizer' => 'nullable|string|max:256',
            'organizer_phone' => 'required|string|max:256',
            'organizer_social_media' => 'nullable|url',
            'judge' => 'nullable|string|max:256',
            'disciplines' => 'required|array',
            'registration_begin_date' => 'required|date_format:"Y-m-d"|before_or_equal:begin_date',
            'registration_end_date' => 'required|date_format:"Y-m-d"|after_or_equal:registration_begin_date|before_or_equal:begin_date',
            'sponsor'=>'nullable|string|max:1024',
            'sponsor_logo' => 'nullable|url'
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $competition = Competition::find($id);
        if (!$competition) {
            throw new ModelNotFoundException('Competition not found');
        }

        $competition->update($request->all());
        $competition->save();

        return response()->json($competition)->setStatusCode(200);
    }

}
