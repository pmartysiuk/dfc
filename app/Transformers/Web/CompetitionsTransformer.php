<?php

namespace App\Transformers\Web;

class CompetitionsTransformer extends Transformer
{

    public function transform(array $competition)
    {
        
        return [
            'id' => $competition['id'],
            'created_by' => $competition['created_by'],
            'title' => $competition['title'],
            'organizer' => $competition['organizer'],
            'organizer_phone' => $competition['organizer_phone'],
            'organizer_social_media' => $competition['organizer_social_media'],
            'judge' => $competition['judge'],
            'city' => $competition['city'],
            'address' => $competition['address'],
            'description' => $competition['description'],
            'status' => $competition['status'],
            'sponsor' => $competition['sponsor'],
            'sponsor_logo' => $competition['sponsor_logo'],
            'type' => $competition['type'],
            'registration_begin_date' => $competition['registration_begin_date'],
            'registration_end_date' => $competition['registration_end_date'],
            'begin_date' => $competition['begin_date'],
            'end_date' => $competition['end_date']



//            {{date('j', strtotime($competition['begin_date']))}} {{trans('date.' . date('F', strtotime($competition['begin_date'])))}}&nbsp;&mdash;&nbsp;

        ];
    }
}
