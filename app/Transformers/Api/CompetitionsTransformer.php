<?php

namespace App\Transformers\Api;

class CompetitionsTransformer extends Transformer
{
    private $applicationsTransformer;

    public function __construct(ApplicationsTransformer $applicationsTransformer)
    {
        $this->applicationsTransformer = $applicationsTransformer;
    }

    public function transform($competition)
    {
        $disciplines = [];
        foreach ($competition->disciplines as $discipline) {
            $disciplines[] = $discipline->toArray();
        }

        return [
            'id' => $competition->id,
            'created_by' => $competition->created_by,
            'title' => $competition->title,
            'organizer' => $competition->organizer,
            'organizer_phone' => $competition->organizer_phone,
            'organizer_social_media' => json_decode($competition->organizer_social_media, true),
            'judge' => json_decode($competition->judge, true),
            'city' => $competition->city,
            'address' => $competition->address,
            'coordinates' => json_decode($competition->coordinates, true),
            'place' => $competition->place,
            'description' => $competition->description,
            'applications' => $this->applicationsTransformer->transformCollection($competition->applications),
            'disciplines' => $disciplines,
            'status' => $competition->status,
            'sponsor' => json_decode($competition->sponsor, true),
            'type' => $competition->type->title,
            'registration_begin_date' => $competition->registration_begin_date,
            'registration_end_date' => $competition->registration_end_date,
            'begin_date' => $competition->begin_date,
            'end_date' => $competition->end_date
        ];
    }
}
