<?php

namespace App\Transformers\Api;

class ApplicationsTransformer extends Transformer
{

    public function transform($application)
    {
        $disciplines = [];
        foreach ($application->disciplines as $discipline) {
            $disciplines[] = $discipline->toArray();
        }

        return [
            'id' => $application->id,
            'applicant_name' => $application->applicant_name,
            'applicant_email' => $application->applicant_email,
            'applicant_phone' => $application->applicant_phone,
            'applicant_birth_date' => $application->applicant_birth_date,
            'pet_name' => $application->info['pet_name'],
            'pet_breed' => $application->info['pet_breed'],
            'pet_birth_date' => $application->info['pet_birth_date'],
            'comment' => $application->comment,
            'disciplines' => $disciplines,
            'created_at' => $application->created_at->toDateTimeString()
        ];
    }
}
