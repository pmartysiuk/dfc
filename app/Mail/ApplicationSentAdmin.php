<?php

namespace App\Mail;

use App\Entities\Application;
use App\Entities\Competition;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApplicationSentAdmin extends Mailable
{
    use Queueable, SerializesModels;

    private $competition, $application;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Application $application, Competition $competition)
    {
        $this->application = $application;
        $this->competition = $competition;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Новая заявка на соревнование " . $this->competition->title)
            ->to($this->application->applicant_email)
            ->view('emails.applications.sent_admin', ['application' => $this->application, 'competition' => $this->competition]);
    }
}
