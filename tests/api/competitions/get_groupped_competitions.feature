Feature: Get competitions filtered by status

  Scenario: Get actual competitions
    When I send GET request to "competitions?status=actual"
    Then response status code should be 200
      And JSON response body should be like:
        """
          {
              "data": [
                  {
                      "id": 2,
                      "begin_date": "2018-01-11 11:00:00"
                  },
                  {
                      "id": 4,
                      "begin_date": "2018-01-11 11:00:00"
                  },
                  {
                      "id": 1,
                      "begin_date": "2018-02-11 11:00:00"
                  },
                  {
                      "id": 3,
                      "begin_date": "2018-02-11 11:00:00"
                  },
                  {
                      "id": 6,
                      "begin_date": "2018-03-11 11:00:00"
                  },
                  {
                      "id": 7,
                      "begin_date": "2018-03-11 11:00:00"
                  },
                  {
                      "id": 9,
                      "begin_date": "2018-03-11 11:00:00"
                  },
                  {
                      "id": 10,
                      "begin_date": "2018-03-11 11:00:00"
                  },
                  {
                      "id": 5,
                      "begin_date": "2018-04-11 11:00:00"
                  },
                  {
                      "id": 8,
                      "begin_date": "2018-04-11 11:00:00"
                  }
              ],
              "total": 10
          }
        """

  Scenario: Get archived competitions
    When I send GET request to "competitions?status=archived"
    Then response status code should be 200
      And JSON response body should be like:
        """
          {
              "data": [
                  {
                      "id": 12,
                      "begin_date": "2018-03-11 11:00:00"
                  },
                  {
                      "id": 11,
                      "begin_date": "2018-04-11 11:00:00"
                  }
              ],
              "total": 2
          }
        """