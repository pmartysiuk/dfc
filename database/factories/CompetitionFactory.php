<?php

use Faker\Generator as Faker;

$factory->define(App\Competition::class, function (Faker $faker) {
    return [
        'created_by'              => $faker->numberBetween(1, 10),
        'organizer'               => $faker->name('M'),
        'organizer_phone'         => $faker->phoneNumber(),
        'organizer_social_media'  => $faker->url(),
        'judge'                   => $faker->name('M'),
        'place'                   => $faker->city(),
        'description'             => $faker->paragraph(4),
        'status'                  => $faker->numberBetween(1, 6),
        'sponsor'                 => $faker->name(),
        'sponsor_logo'            => $faker->image(),
        'registration_begin_date' => $faker->dateTime('now'),
        'registration_end_date'   => $faker->dateTime('now'),
        'begin_date'              => $faker->dateTime('now'),
        'end_date'                => $faker->dateTime('now'),
    ];
});