<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompetitionDisciplineApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competition_discipline_application', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('competition_id');
            $table->bigInteger('discipline_id');
            $table->bigInteger('application_id');
            $table->unique(['competition_id', 'discipline_id', 'application_id'], "cda");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competition_discipline_application');
    }
}
