<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompetitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competitions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('created_by', false, true)->nullable();
            $table->string('title');
            $table->string('organizer')->nullable();
            $table->string('organizer_phone')->nullable();
            $table->text('organizer_social_media')->nullable();
            $table->string('judge')->nullable();
            $table->string('city');
            $table->string('address')->nullable();
            $table->string('place')->nullable();
            $table->string('coordinates')->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('status', false, true);
            $table->bigInteger('type_id', false, true)->nullable();
            $table->text('sponsor')->nullable();
            $table->timestamp('registration_begin_date')->nullable();
            $table->timestamp('registration_end_date')->nullable();
            $table->timestamp('begin_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competitions');
    }
}
