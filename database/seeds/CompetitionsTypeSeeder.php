<?php

use Illuminate\Database\Seeder;

class CompetitionsTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('competition_types')->truncate();

        DB::table('competition_types')
            ->insert([
                    [
                        'id' => 1,
                        'title' => "Рейтинговое соревнование"
                    ],
                    [
                        'id' => 2,
                        'title' => "Развлекательное соревнование"
                    ],
                    [
                        'id' => 3,
                        'title' => "Чемпионат РБ"
                    ],
                ]
            );
    }
}